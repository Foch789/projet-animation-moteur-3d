attribute highp vec4 posAttr;
attribute lowp vec4 colAttr;
attribute mediump vec4 texAttr;
attribute float exploAttr;
attribute highp vec3 norAttr;

varying lowp vec4 col;
varying mediump vec4 texc;
varying float explo;
varying highp vec3 nor;

uniform highp mat4 matrix;
uniform highp mat4 projMatrix;  // projection matrix
uniform highp mat4 mvMatrix;    // model-view matrix
uniform highp mat3 norMatrix;   // normal matrix 3x3





void main() {
   texc = texAttr;
   col = colAttr;
   explo = exploAttr;

   gl_Position = projMatrix * mvMatrix * posAttr;
   nor = norMatrix * norAttr;
}

