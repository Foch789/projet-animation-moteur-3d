// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    int get_vitesse_aplha() const {return m_vitesse_alpha;}
    ~GLArea();

public slots:
    void setRadius(double radius);
    void set_vitesse_change(int vitesse);
    void demarrer();
    void stop();

signals:  // On ne les implémente pas, elles seront générées par MOC ;
          // les paramètres seront passés aux slots connectés.
    void radiusChanged(double newRadius);
    void vitesse_change(int vitesse);

protected slots:
    void onTimeout();

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

    void paintGL_vertex_color_triangle(QOpenGLBuffer &vertex,QOpenGLBuffer &color, QOpenGLBuffer &normal, int debut, int taille);
    void paintGL_vertex_color_rectangle(QOpenGLBuffer &vertex,QOpenGLBuffer &color, QOpenGLBuffer &normal, int debut, int taille);

    void paintGL_vertex_text_triangle(QOpenGLBuffer &vertex,QOpenGLBuffer &text, QOpenGLBuffer &normal, int debut, int taille, int nbText);
    void paintGL_vertex_text_rectangle(QOpenGLBuffer &vertex,QOpenGLBuffer &text, QOpenGLBuffer &normal, QOpenGLBuffer &explo, int debut, int taille, int nbText);

    void paintGL_vertex_color_vilebrequin(QMatrix4x4 &matrix, QMatrix4x4 &cam);
    void paintGL_vertex_color_piston(QMatrix4x4 &matrix, QMatrix4x4 &cam, GLfloat deg, int id_piston);
    void paintGL_vertex_color_cylindre(QMatrix4x4 &matrix, QMatrix4x4 &cam);


private:
    double m_angle = 0;
    QTimer *m_timer = nullptr;
    double m_anim = 0;
    double m_alpha = 0.0f;
    int m_vitesse_alpha = 2;
    double m_radius = 0.5;
    double m_ratio = 1;

    QOpenGLShaderProgram *m_program;

    void makeGLObjects();
    void tearGLObjects();

    QOpenGLTexture *m_textures[1];
//    QOpenGLBuffer m_vertexBuffer_triangle_vbo;
//    QOpenGLBuffer m_vertexBuffer_rectangle_vbo;
//    QOpenGLBuffer m_colorBuffer_triangle_vbo;
//    QOpenGLBuffer m_colorBuffer_rectangle_vbo;

    QOpenGLBuffer m_vertexBuffer_triangle_piston_vbo;
    QOpenGLBuffer m_vertexBuffer_rectangle_piston_vbo;
    QOpenGLBuffer m_colorBuffer_triangle_piston_vbo;
    QOpenGLBuffer m_colorBuffer_rectangle_piston_vbo;
    QOpenGLBuffer m_normalBuffer_triangle_piston_vbo;
    QOpenGLBuffer m_normalBuffer_rectangle_piston_vbo;

    QOpenGLBuffer m_vertexBuffer_triangle_cylindre_vbo;
    QOpenGLBuffer m_vertexBuffer_rectangle_cylindre_vbo;
    QOpenGLBuffer m_textureBuffer_triangle_cylindre_vbo;
    QOpenGLBuffer m_textureBuffer_rectangle_cylindre_vbo;
    QOpenGLBuffer m_colorBuffer_triangle_cylindre_vbo;
    QOpenGLBuffer m_colorBuffer_rectangle_cylindre_vbo;
    QOpenGLBuffer m_exploBuffer_rectangle_cylindre_vbo;
    QOpenGLBuffer m_normalBuffer_triangle_cylindre_vbo;
    QOpenGLBuffer m_normalBuffer_rectangle_cylindre_vbo;

    QOpenGLBuffer m_vertexBuffer_triangle_vilebrequin_vbo;
    QOpenGLBuffer m_vertexBuffer_rectangle_vilebrequin_vbo;
    QOpenGLBuffer m_colorBuffer_triangle_vilebrequin_vbo;
    QOpenGLBuffer m_colorBuffer_rectangle_vilebrequin_vbo;
    QOpenGLBuffer m_normalBuffer_triangle_vilebrequin_vbo;
    QOpenGLBuffer m_normalBuffer_rectangle_vilebrequin_vbo;

};




#endif // GLAREA_H
