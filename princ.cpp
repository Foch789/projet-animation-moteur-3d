// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "princ.h"
#include <QDebug>
#include "dialog.h"

Princ::Princ(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    connect (glarea, SIGNAL(radiusChanged(double)), this, SLOT(setSliderRadius(double)));
    connect (sli_radius, SIGNAL(valueChanged(int)), this, SLOT(onSliderRadius(int)));
    connect(btn_param, SIGNAL(clicked()), this, SLOT(openDialog()));
}


void Princ::setSliderRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    int value = radius*20;
    if (sli_radius->value() != value) {
        qDebug() << "  sli_radius->setvalue()";
        sli_radius->setValue(value);
        sli_radius->setSliderPosition(value);
    }
}

void Princ::openDialog()
{
    Dialog dialogue(this);
    dialogue.setSliderVitesse(glarea->get_vitesse_aplha());
    connect(&dialogue, &Dialog::demarrerMoteur, glarea, &GLArea::demarrer);
    connect(&dialogue, &Dialog::stopperMoteur, glarea, &GLArea::stop);
    connect(&dialogue, &Dialog::vitesseChange, glarea, &GLArea::set_vitesse_change);
    dialogue.exec();
}

void Princ::onSliderRadius(int value)
{
    qDebug() << __FUNCTION__ << value << sender();
    qDebug() << "  emit radiusChanged()";
    emit glarea->radiusChanged(value/20.0);
}
