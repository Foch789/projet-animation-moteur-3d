#include "dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);

    connect(demaMoteur, SIGNAL(clicked()), this, SIGNAL(demarrerMoteur()));
    connect(stopMoteur, SIGNAL(clicked()), this, SIGNAL(stopperMoteur()));
    connect(vitesse_slider, SIGNAL(valueChanged(int)), this, SIGNAL(vitesseChange(int)));
}

void Dialog::setSliderVitesse(int nb)
{
    vitesse_slider->setSliderPosition(nb);
}
