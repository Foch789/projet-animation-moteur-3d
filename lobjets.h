#ifndef LOBJETS_H
#define LOBJETS_H

#include <QVector>
#include <QOpenGLWidget>

struct LTriangle{
    QVector<GLfloat> vertices;
    QVector<GLfloat> colors;
    QVector<GLfloat> textures;
    QVector<GLfloat> normals;
    int nbVertices = 0;
    QVector<int> stopModel;

    LTriangle operator+=(LTriangle array)
    {
        vertices += array.vertices;
        colors += array.colors;
        textures += array.textures;
        normals += array.normals;
        nbVertices += array.nbVertices;
        return *this;
    }
};

struct LRectangle{
    QVector<GLfloat> vertices;
    QVector<GLfloat> colors;
    QVector<GLfloat> textures;
    QVector<GLfloat> normals;
    QVector<GLfloat> concernExplosion;
    int nbVertices = 0;
    QVector<int> stopModel;

    LRectangle operator+=(LRectangle array)
    {
        vertices += array.vertices;
        colors += array.colors;
        textures += array.textures;
        normals += array.normals;
        concernExplosion += array.concernExplosion;
        nbVertices += array.nbVertices;
        return *this;
    }
};

struct LCouleur{
    GLfloat r,g,b;

    LCouleur operator*(GLfloat nb)
    {
        LCouleur l = *this;
        l.r *= nb;
        l.g *= nb;
        l.b *= nb;
        return l;
    }

    LCouleur operator*=(GLfloat nb)
    {
        r *= nb;
        g *= nb;
        b *= nb;
        return *this;
    }

    LCouleur operator/(GLfloat nb)
    {
        LCouleur l = *this;
        l.r /= nb;
        l.g /= nb;
        l.b /= nb;
        return l;
    }

    LCouleur operator/=(GLfloat nb)
    {
        r /= nb;
        g /= nb;
        b /= nb;
        return *this;
    }
};

struct LForme{
    LTriangle forme_triangle;
    LRectangle forme_rectangle;
};

struct LVertex{
    GLfloat x,y,z;
};

LForme creer_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_cyl, LCouleur color);
LTriangle creer_face_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_rec, LCouleur color);
LRectangle creer_cote_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_cyl, LCouleur color);

LForme creer_demi_cylindre_text(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl);
LForme creer_face_demi_cylindre_text(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl);
LRectangle creer_cote_demi_cylindre_text(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl);

LForme creer_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color);
LForme creer_face_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color);
LRectangle creer_cote_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color);

LRectangle creer_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color);
LRectangle creer_face_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color);
LRectangle creer_cote_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color);

/*
struct DataVilebrequin{
    QVector<LForme> axeDebutFins;
    QVector<LForme> axes;
    QVector<LRectangle> separateurs;
};

struct DataCylindre{
    QVector<LForme> cylindres;
};

struct DataPistonBielle{
    QVector<LForme> pistons;
    QVector<LRectangle> bielles;
};

struct MoteurFlat{
    DataVilebrequin vilebrequin;
    DataCylindre cylindres;
    DataPistonBielle piston;
    int couple = 1;
};*/

#endif // LOBJETS_H
