#ifndef DIALOG_H
#define DIALOG_H

#include "ui_dialog.h"

class Dialog : public QDialog, private Ui::Dialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    void setSliderVitesse(int nb);

signals:
    void demarrerMoteur();
    void stopperMoteur();
    void vitesseChange(int nb);
};

#endif // DIALOG_H
