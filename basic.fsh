varying lowp vec4 col;
varying mediump vec4 texc;
varying float explo;
uniform sampler2D texture;
uniform int textureSample;
uniform float changeColor;
varying highp vec3 nor;

void main() {

    // Direction de lumière normalisée
    vec3 light = normalize(vec3(0.0, 0.0, 10.0));

    // Normale du fragment, normalisée
    vec3 nor3 = normalize(nor);

    // Cosinus de l'angle entre la normale et la lumière
    // borné entre 0 et 1
    float cosTheta = clamp( dot(nor3,light ), 0.3 , 1.0 );

    // Couleur de la lumière
    vec3 lightColor = vec3 (1.0, 1.0, 1.0);

    vec3 ambiantLight = vec3 (0.3, 0.3, 0.3);
    vec3 sumLight = lightColor * cosTheta + ambiantLight;

    if(textureSample == 1)
    {
        vec4 tex = texture2D(texture, texc.st);
        if(explo < 1.f)
        {
            //gl_FragColor = tex;
            gl_FragColor = tex * vec4(clamp(sumLight, 0.0, 1.0), 1.0);
        }
        else
        {
            //gl_FragColor =  tex + vec4(changeColor, 0.f, 0.f, 0.f)*tex.a;
            gl_FragColor = (tex + vec4(changeColor, 0.f, 0.f, 0.f)) * vec4(clamp(sumLight, 0.0, 1.0), 1.0);
        }
    }
    else
    {
        gl_FragColor = vec4(clamp(sumLight, 0.0, 1.0), 1.0) * col;
       //gl_FragColor = vec4(lightColor * cosTheta, 1.0) * col;
    }
}

