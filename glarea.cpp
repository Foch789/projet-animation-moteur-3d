// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <QVector3D>
#include <lobjets.h>
#include <math.h>

#define PI 3.141592f

QVector<GLfloat> temp_sup;
QVector<int> compteur;
QVector<bool> test;

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";

LTriangle total_tri_vilebrequin;
LRectangle total_rec_vilebrequin;

LTriangle total_tri_piston;
LRectangle total_rec_piston;

LTriangle total_tri_cylindre;
LRectangle total_rec_cylindre;

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
                        // cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(50);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));

    setRadius(0.6);
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();
    tearGLObjects();
    doneCurrent();
}

void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);

    makeGLObjects();

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    m_program->setUniformValue("texture", 0);
}

void GLArea::makeGLObjects()
{
    LCouleur color, color2, color3, color4, color5;
    color.r = 221.f/255.f;
    color.g = 0.f;
    color.b = 0.f;

    color2.r = 0.0f;
    color2.g = 1.0f;
    color2.b = 0.0f;

    color3.r = 0.0f;
    color3.g = 0.0f;
    color3.b = 1.0f;

    color4.r = 0.5f;
    color4.g = 0.5f;
    color4.b = 0.5f;

    color5.r = 255.f/255.f;
    color5.g = 200.f/255.f;
    color5.b = 0.f/255.f;

    //En fonction
    LForme axeDebutFin = creer_cylindre(40, 0.5f, 0.2f, color4);

    total_tri_vilebrequin += axeDebutFin.forme_triangle;
    total_rec_vilebrequin += axeDebutFin.forme_rectangle;
    total_tri_vilebrequin.stopModel.append(total_tri_vilebrequin.nbVertices);
    total_rec_vilebrequin.stopModel.append(total_rec_vilebrequin.nbVertices);

    LForme axe = creer_cylindre(10, 0.1f, 0.2f, color);

    total_tri_vilebrequin += axe.forme_triangle;
    total_rec_vilebrequin += axe.forme_rectangle;
    total_tri_vilebrequin.stopModel.append(total_tri_vilebrequin.nbVertices);
    total_rec_vilebrequin.stopModel.append(total_rec_vilebrequin.nbVertices);

    LRectangle separateur = creer_rectangle(0.6f, 0.22f, 0.1f, color2);

    total_rec_vilebrequin += separateur;
    total_rec_vilebrequin.stopModel.append(total_rec_vilebrequin.nbVertices);

    //Fonction
    if (m_vertexBuffer_triangle_vilebrequin_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_triangle_vilebrequin_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_triangle_vilebrequin_vbo.allocate(total_tri_vilebrequin.vertices.constData(), total_tri_vilebrequin.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_triangle_vilebrequin_vbo.create();
    m_colorBuffer_triangle_vilebrequin_vbo.bind();
    m_colorBuffer_triangle_vilebrequin_vbo.allocate(total_tri_vilebrequin.colors.constData(), total_tri_vilebrequin.colors.count() * sizeof(GLfloat));

    m_normalBuffer_triangle_vilebrequin_vbo.create();
    m_normalBuffer_triangle_vilebrequin_vbo.bind();
    m_normalBuffer_triangle_vilebrequin_vbo.allocate(total_tri_vilebrequin.normals.constData(), total_tri_vilebrequin.normals.count() * sizeof(GLfloat));

    if (m_vertexBuffer_rectangle_vilebrequin_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_rectangle_vilebrequin_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_rectangle_vilebrequin_vbo.allocate(total_rec_vilebrequin.vertices.constData(), total_rec_vilebrequin.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_rectangle_vilebrequin_vbo.create();
    m_colorBuffer_rectangle_vilebrequin_vbo.bind();
    m_colorBuffer_rectangle_vilebrequin_vbo.allocate(total_rec_vilebrequin.colors.constData(), total_rec_vilebrequin.colors.count() * sizeof(GLfloat));

    m_normalBuffer_rectangle_vilebrequin_vbo.create();
    m_normalBuffer_rectangle_vilebrequin_vbo.bind();
    m_normalBuffer_rectangle_vilebrequin_vbo.allocate(total_rec_vilebrequin.normals.constData(), total_rec_vilebrequin.normals.count() * sizeof(GLfloat));

    //Fonction
    LForme piston = creer_cylindre(10, 0.1f, 0.4f, color5);

    total_tri_piston += piston.forme_triangle;
    total_rec_piston += piston.forme_rectangle;
    total_tri_piston.stopModel.append(total_tri_piston.nbVertices);
    total_rec_piston.stopModel.append(total_rec_piston.nbVertices);

    LRectangle bielle = creer_rectangle(0.8f, 0.1f, 0.05f, color3);
    total_rec_piston += bielle;
    total_rec_piston.stopModel.append(total_rec_piston.nbVertices);

    //Fonction
    if (m_vertexBuffer_triangle_piston_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_triangle_piston_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_triangle_piston_vbo.allocate(total_tri_piston.vertices.constData(), total_tri_piston.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_triangle_piston_vbo.create();
    m_colorBuffer_triangle_piston_vbo.bind();
    m_colorBuffer_triangle_piston_vbo.allocate(total_tri_piston.colors.constData(), total_tri_piston.colors.count() * sizeof(GLfloat));

    m_normalBuffer_triangle_piston_vbo.create();
    m_normalBuffer_triangle_piston_vbo.bind();
    m_normalBuffer_triangle_piston_vbo.allocate(total_tri_piston.normals.constData(), total_tri_piston.normals.count() * sizeof(GLfloat));

    if (m_vertexBuffer_rectangle_piston_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_rectangle_piston_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_rectangle_piston_vbo.allocate(total_rec_piston.vertices.constData(), total_rec_piston.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_rectangle_piston_vbo.create();
    m_colorBuffer_rectangle_piston_vbo.bind();
    m_colorBuffer_rectangle_piston_vbo.allocate(total_rec_piston.colors.constData(), total_rec_piston.colors.count() * sizeof(GLfloat));

    m_normalBuffer_rectangle_piston_vbo.create();
    m_normalBuffer_rectangle_piston_vbo.bind();
    m_normalBuffer_rectangle_piston_vbo.allocate(total_rec_piston.normals.constData(), total_rec_piston.normals.count() * sizeof(GLfloat));

    // Cylindre
    LForme cylindre = creer_demi_cylindre(10, 0.1f, 0.8f, 0.02f, color3);
    //LForme cylindre = creer_demi_cylindre_text(10, 0.1f, 0.8f, 0.02f);

    total_tri_cylindre += cylindre.forme_triangle;
    total_rec_cylindre += cylindre.forme_rectangle;
    total_tri_cylindre.stopModel.append(total_tri_cylindre.nbVertices);
    total_rec_cylindre.stopModel.append(total_rec_cylindre.nbVertices);

    if (m_vertexBuffer_triangle_cylindre_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_triangle_cylindre_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_triangle_cylindre_vbo.allocate(total_tri_cylindre.vertices.constData(), total_tri_cylindre.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_triangle_cylindre_vbo.create();
    m_colorBuffer_triangle_cylindre_vbo.bind();
    m_colorBuffer_triangle_cylindre_vbo.allocate(total_tri_cylindre.colors.constData(), total_tri_cylindre.colors.count() * sizeof(GLfloat));

    m_normalBuffer_triangle_cylindre_vbo.create();
    m_normalBuffer_triangle_cylindre_vbo.bind();
    m_normalBuffer_triangle_cylindre_vbo.allocate(total_tri_cylindre.normals.constData(), total_tri_cylindre.normals.count() * sizeof(GLfloat));

    if (m_vertexBuffer_rectangle_cylindre_vbo.create()) qDebug() << "Success creating vertex position buffer";
    if (m_vertexBuffer_rectangle_cylindre_vbo.bind()) qDebug() << "Success biding vertex position buffer";
    m_vertexBuffer_rectangle_cylindre_vbo.allocate(total_rec_cylindre.vertices.constData(), total_rec_cylindre.vertices.count() * sizeof(GLfloat));

    m_colorBuffer_rectangle_cylindre_vbo.create();
    m_colorBuffer_rectangle_cylindre_vbo.bind();
    m_colorBuffer_rectangle_cylindre_vbo.allocate(total_rec_cylindre.colors.constData(), total_rec_cylindre.colors.count() * sizeof(GLfloat));

    m_normalBuffer_rectangle_cylindre_vbo.create();
    m_normalBuffer_rectangle_cylindre_vbo.bind();
    m_normalBuffer_rectangle_cylindre_vbo.allocate(total_rec_cylindre.normals.constData(), total_rec_cylindre.normals.count() * sizeof(GLfloat));

    QVector<GLfloat> textures_tri;

    for (int i = 0 ;i < 10 ;i++) {
        textures_tri.append(0.0);
        textures_tri.append(0.0);

        textures_tri.append(0.5);
        textures_tri.append(1.0);

        textures_tri.append(1.0);
        textures_tri.append(0.0);
    }

    QVector<GLfloat> test_explo;
     for (int i = 0 ;i < total_rec_cylindre.vertices.count()/3 ;i++) {
         test_explo.append(2.0f);
     }

    m_textureBuffer_triangle_cylindre_vbo.create();
    m_textureBuffer_triangle_cylindre_vbo.bind();
    m_textureBuffer_triangle_cylindre_vbo.allocate(textures_tri.constData(), textures_tri.count() * sizeof(GLfloat));

    m_textureBuffer_rectangle_cylindre_vbo.create();
    m_textureBuffer_rectangle_cylindre_vbo.bind();
    m_textureBuffer_rectangle_cylindre_vbo.allocate(total_rec_cylindre.textures.constData(), total_rec_cylindre.textures.count() * sizeof(GLfloat));

    m_exploBuffer_rectangle_cylindre_vbo.create();
    m_exploBuffer_rectangle_cylindre_vbo.bind();
    m_exploBuffer_rectangle_cylindre_vbo.allocate(total_rec_cylindre.concernExplosion.constData(), total_rec_cylindre.concernExplosion.count() * sizeof(GLfloat));
    //m_exploBuffer_rectangle_cylindre_vbo.allocate(test_explo.constData(), test_explo.count() * sizeof(GLfloat));

    QString nom = QString(":/metal.jpeg");
    QImage image(nom);
    if (image.isNull()) qDebug() << "load image" << nom << "failed";
    else qDebug() << "load image" << nom << image.size();
    m_textures[0] = new QOpenGLTexture(image);
}

void GLArea::tearGLObjects()
{
    m_vertexBuffer_triangle_piston_vbo.destroy();
    m_vertexBuffer_rectangle_piston_vbo.destroy();
    m_colorBuffer_triangle_piston_vbo.destroy();
    m_colorBuffer_rectangle_piston_vbo.destroy();
    m_normalBuffer_triangle_piston_vbo.destroy();
    m_normalBuffer_rectangle_piston_vbo.destroy();

    m_vertexBuffer_triangle_cylindre_vbo.destroy();
    m_vertexBuffer_rectangle_cylindre_vbo.destroy();
    m_textureBuffer_triangle_cylindre_vbo.destroy();
    m_textureBuffer_rectangle_cylindre_vbo.destroy();
    m_exploBuffer_rectangle_cylindre_vbo.destroy();
    m_colorBuffer_triangle_cylindre_vbo.destroy();
    m_colorBuffer_rectangle_cylindre_vbo.destroy();
    m_normalBuffer_triangle_cylindre_vbo.destroy();
    m_normalBuffer_rectangle_cylindre_vbo.destroy();

    m_vertexBuffer_triangle_vilebrequin_vbo.destroy();
    m_vertexBuffer_rectangle_vilebrequin_vbo.destroy();
    m_colorBuffer_triangle_vilebrequin_vbo.destroy();
    m_colorBuffer_rectangle_vilebrequin_vbo.destroy();
    m_normalBuffer_triangle_vilebrequin_vbo.destroy();
    m_normalBuffer_rectangle_vilebrequin_vbo.destroy();

    delete m_textures[0];

}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    // doProjection();
}

void GLArea::paintGL()
{
    qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program

    QMatrix4x4 proj_mat;
    GLfloat hr = m_radius, wr = hr * m_ratio;
    proj_mat.frustum(-wr, wr, -hr, hr, 1.0, 5.0);

    QMatrix4x4 cam_mat;
    cam_mat.translate(0, 0, -3.0);

    QMatrix4x4 world_mat;
    world_mat.rotate(m_angle, 0, 1, 0);
    //world_mat.translate(0.f, 0.5f, 0.f);

    QMatrix3x3 normal_mat = world_mat.normalMatrix();


    m_program->setUniformValue("projMatrix", proj_mat);
    m_program->setUniformValue("mvMatrix", cam_mat*world_mat);
    m_program->setUniformValue("norMatrix", normal_mat);


    //Debut chantier

    //Vilebrequin
    QMatrix4x4 matrixCopyVilebrequin = world_mat;
    matrixCopyVilebrequin.rotate(90, 0, 1, 0);
    matrixCopyVilebrequin.rotate(90, 0, 0, 1);
    matrixCopyVilebrequin.translate(0.f, 0.f, -1.f);
    matrixCopyVilebrequin.rotate(m_alpha, 0.f, 0.f, 1.f);

    QMatrix3x3 normal_vile = matrixCopyVilebrequin.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyVilebrequin);
    m_program->setUniformValue("norMatrix", normal_vile);

    paintGL_vertex_color_vilebrequin(matrixCopyVilebrequin, cam_mat);

    matrixCopyVilebrequin.translate(0.f, 0.f, -0.1f);
    normal_vile = matrixCopyVilebrequin.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyVilebrequin);
    m_program->setUniformValue("norMatrix", normal_vile);

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  0, total_tri_vilebrequin.stopModel[1]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   0, total_rec_vilebrequin.stopModel[1]);

    //Piston
    QMatrix4x4 matrixCopyPiston = world_mat;
    //matrixCopyPiston.translate(-0.6f, 0.f, 1.2f);
    matrixCopyPiston.translate(-0.6f, -1.2f, 0.f);
    matrixCopyPiston.rotate(90, 1, 0, 0);
    QMatrix3x3 normal_piston = matrixCopyPiston.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyPiston);
    m_program->setUniformValue("norMatrix", normal_piston);

    temp_sup.append(0.f);
    compteur.append(1);
    test.append(false);
    paintGL_vertex_color_piston(matrixCopyPiston, cam_mat, 0, 0);

    //Cylindre
    QMatrix4x4 matrixCopyCylindre = world_mat;
    matrixCopyCylindre.translate(-0.6f, 0.f, 0.f);
    matrixCopyCylindre.rotate(180, 0, 0, 1);
    matrixCopyCylindre.rotate(-90, 1, 0, 0);
    matrixCopyCylindre.translate(0.f, 0.f, 1.f);
    QMatrix3x3 normal_cylindre = matrixCopyCylindre.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyCylindre);
    m_program->setUniformValue("norMatrix", normal_cylindre);

    paintGL_vertex_color_cylindre(matrixCopyCylindre, cam_mat);

    //Piston2
    matrixCopyPiston = world_mat;
    matrixCopyPiston.translate(0.0f, 1.2f, 0.f);
    matrixCopyPiston.rotate(-90, 1, 0, 0);
    matrixCopyPiston.rotate(0, 0, 0, 1);
    normal_piston = matrixCopyPiston.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyPiston);
    m_program->setUniformValue("norMatrix", normal_piston);

    temp_sup.append(0.f);
    compteur.append(0);
    test.append(false);
    paintGL_vertex_color_piston(matrixCopyPiston, cam_mat, 0, 1);

    //Cylindre2
    matrixCopyCylindre = world_mat;
    matrixCopyCylindre.translate(0.0f, 0.f, 0.f);
    //matrixCopyCylindre.rotate(180, 0, 0, 1);
    matrixCopyCylindre.rotate(-90, 1, 0, 0);
    matrixCopyCylindre.translate(0.f, 0.f, 1.f);
    normal_cylindre = matrixCopyCylindre.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyCylindre);
    m_program->setUniformValue("norMatrix", normal_cylindre);

    paintGL_vertex_color_cylindre(matrixCopyCylindre, cam_mat);

    world_mat.translate(1.2f, 0.0f, 0.f);
    normal_mat = world_mat.normalMatrix();

    m_program->setUniformValue("mvMatrix", cam_mat*world_mat);
    m_program->setUniformValue("norMatrix", normal_mat);

    //Vilebrequin2
    matrixCopyVilebrequin = world_mat;
    matrixCopyVilebrequin.rotate(90, 0, 1, 0);
    matrixCopyVilebrequin.rotate(180, 0, 0, 1);
    matrixCopyVilebrequin.translate(0.f, 0.f, -1.f);
    matrixCopyVilebrequin.rotate(m_alpha, 0.f, 0.f, 1.f);

    normal_vile = matrixCopyVilebrequin.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyVilebrequin);
    m_program->setUniformValue("norMatrix", normal_vile);

    paintGL_vertex_color_vilebrequin(matrixCopyVilebrequin, cam_mat);

    matrixCopyVilebrequin.translate(0.f,0.f,1.3f);
    normal_vile = matrixCopyVilebrequin.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyVilebrequin);
    m_program->setUniformValue("norMatrix", normal_vile);

    //CylindreFin
    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

    //Piston
    matrixCopyPiston = world_mat;
    //matrixCopyPiston.translate(-0.6f, 0.f, 1.2f);
    matrixCopyPiston.translate(-0.6f, -1.2f, 0.f);
    matrixCopyPiston.rotate(90, 1, 0, 0);
    normal_piston = matrixCopyPiston.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyPiston);
    m_program->setUniformValue("norMatrix", normal_piston);

    temp_sup.append(0.f);
    compteur.append(1);
    test.append(false);
    paintGL_vertex_color_piston(matrixCopyPiston, cam_mat, 90, 2);

    //Cylindre
    matrixCopyCylindre = world_mat;
    matrixCopyCylindre.translate(-0.6f, 0.f, 0.f);
    matrixCopyCylindre.rotate(180, 0, 0, 1);
    matrixCopyCylindre.rotate(-90, 1, 0, 0);
    matrixCopyCylindre.translate(0.f, 0.f, 1.f);
    normal_cylindre = matrixCopyCylindre.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyCylindre);
    m_program->setUniformValue("norMatrix", normal_cylindre);

    paintGL_vertex_color_cylindre(matrixCopyCylindre, cam_mat);

    //Piston2
    matrixCopyPiston = world_mat;
    matrixCopyPiston.translate(0.0f, 1.2f, 0.f);
    matrixCopyPiston.rotate(-90, 1, 0, 0);
    matrixCopyPiston.rotate(0, 0, 0, 1);
    normal_piston = matrixCopyPiston.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyPiston);
    m_program->setUniformValue("norMatrix", normal_piston);

    temp_sup.append(0.f);
    compteur.append(0);
    test.append(false);
    paintGL_vertex_color_piston(matrixCopyPiston, cam_mat, 90, 3);

    //Cylindre2
    matrixCopyCylindre = world_mat;
    matrixCopyCylindre.translate(0.0f, 0.f, 0.f);
    //matrixCopyCylindre.rotate(180, 0, 0, 1);
    matrixCopyCylindre.rotate(-90, 1, 0, 0);
    matrixCopyCylindre.translate(0.f, 0.f, 1.f);
    normal_cylindre = matrixCopyCylindre.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam_mat*matrixCopyCylindre);
    m_program->setUniformValue("norMatrix", normal_cylindre);

    paintGL_vertex_color_cylindre(matrixCopyCylindre, cam_mat);




    m_program->release();
}

void GLArea::paintGL_vertex_color_vilebrequin(QMatrix4x4 &matrix, QMatrix4x4 &cam)
{
    QMatrix4x4 matrixCopy = matrix;

    matrixCopy.translate(0.f,0.f,0.1f);
    QMatrix3x3 normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    matrixCopy = matrix;

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

    //Separateur 1
    matrixCopy.translate(-0.4f,-0.22f/2.f,0.25f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);

    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[1], total_rec_vilebrequin.stopModel[2] - total_rec_vilebrequin.stopModel[1]);

    //Axe 1
    matrixCopy.translate(-0.8f/3.f, 0.f, 0.4f);

    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

    //Separateur 2
    matrixCopy.translate(-0.4f,-0.22f/2.f,0.55f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[1], total_rec_vilebrequin.stopModel[2] - total_rec_vilebrequin.stopModel[1]);

    //Axe 2
    matrixCopy.translate(0.f, 0.f, 0.7f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

    //Separateur 3
    matrixCopy.translate(-0.2f,-0.22f/2.f,0.85f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[1], total_rec_vilebrequin.stopModel[2] - total_rec_vilebrequin.stopModel[1]);

    //Axe 3
    matrixCopy.translate(0.8f/3.f, 0.f, 1.f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

    //Separateur 4
    matrixCopy.translate(-0.2f,-0.22f/2.f,1.15f);
    normal_vile = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_vile);
    //m_program->setUniformValue("mvMatrix", matrixCopy);
    matrixCopy = matrix;

    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
                                   total_rec_vilebrequin.stopModel[1], total_rec_vilebrequin.stopModel[2] - total_rec_vilebrequin.stopModel[1]);

    //Axe 4
//    matrixCopy.translate(0.f, 0.f, 1.3f);
//    normal_vile = matrixCopy.normalMatrix();
//    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
//    m_program->setUniformValue("norMatrix", normal_vile);
//    //m_program->setUniformValue("mvMatrix", matrixCopy);
//    matrixCopy = matrix;

//    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_vilebrequin_vbo, m_colorBuffer_triangle_vilebrequin_vbo, m_normalBuffer_triangle_vilebrequin_vbo,
//                                  total_tri_vilebrequin.stopModel[0], total_tri_vilebrequin.stopModel[1] - total_tri_vilebrequin.stopModel[0]);
//    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_vilebrequin_vbo, m_colorBuffer_rectangle_vilebrequin_vbo, m_normalBuffer_rectangle_vilebrequin_vbo,
//                                   total_rec_vilebrequin.stopModel[0], total_rec_vilebrequin.stopModel[1] - total_rec_vilebrequin.stopModel[0]);

}

void GLArea::paintGL_vertex_color_piston(QMatrix4x4 &matrix, QMatrix4x4 &cam, GLfloat deg, int id_piston)
{
    QMatrix4x4 matrixCopy = matrix;

    if(deg  + m_alpha >= 360)
    {
        deg = (m_alpha+deg) - 360;
    }
    else{
        deg += m_alpha;
    }

    GLfloat pointx = (0.8f/3.f)*cos(deg*(PI/180));
    GLfloat pointy = 0.09f*sin(deg*(PI/180));

    GLfloat pointjx = pointx - 0.3;

    GLfloat distanceJH = sqrt(pow(pointx - (pointjx), 2.0) + pow(pointy - 0, 2.0));
    GLfloat distanceJG = sqrt(pow(pointx - (pointjx), 2.0));

    GLfloat angleHJG = acos(distanceJG/distanceJH)*(180/PI);

    if(deg >= 180.0f) angleHJG = -angleHJG;

    GLfloat truc = (0.8f/3.f)*cos(180.f*(PI/180.f));
    GLfloat temp = 1 - (pointjx/(truc-0.3f));

    if(temp_sup.size() > id_piston)
    {
        //il monte
        if(temp_sup[id_piston] < temp)
        {
            test[id_piston] = false;
            temp_sup[id_piston] = temp;
        }

        //explosion
        if(temp_sup[id_piston] > temp && !test[id_piston])
        {
            compteur[id_piston]+=1;
            if(compteur[id_piston] > 1)
                compteur[id_piston]=0;
            test[id_piston] = true;
        }

        //il descend
        if(temp_sup[id_piston] > temp)
        {
            if(compteur[id_piston]==1)
                m_program->setUniformValue("changeColor", temp);
            temp_sup[id_piston] = temp;
        }
    }


    matrixCopy.translate(0.f,0.f,pointjx);
    QMatrix3x3 normal_piston = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_piston);
    //m_program->setUniformValue("mvMatrix", matrixCopy);

    paintGL_vertex_color_triangle(m_vertexBuffer_triangle_piston_vbo, m_colorBuffer_triangle_piston_vbo, m_normalBuffer_triangle_piston_vbo,
                                  0, total_tri_piston.stopModel[0]);
    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_piston_vbo, m_colorBuffer_rectangle_piston_vbo, m_normalBuffer_rectangle_piston_vbo,
                                   0, total_rec_piston.stopModel[0]);

    matrixCopy.translate(0.f,-0.05f,-0.1f);
    matrixCopy.rotate(90.f, 0.f, 1.0f, 0.f);
    matrixCopy.rotate(-angleHJG, 0.f, 0.0f, 1.0f);
    normal_piston = matrixCopy.normalMatrix();
    m_program->setUniformValue("mvMatrix", cam*matrixCopy);
    m_program->setUniformValue("norMatrix", normal_piston);
    //m_program->setUniformValue("mvMatrix", matrixCopy);

    paintGL_vertex_color_rectangle(m_vertexBuffer_rectangle_piston_vbo, m_colorBuffer_rectangle_piston_vbo, m_normalBuffer_rectangle_piston_vbo,
                                   total_rec_piston.stopModel[0], total_rec_piston.stopModel[1] - total_rec_piston.stopModel[0]);

}

void GLArea::paintGL_vertex_color_cylindre(QMatrix4x4 &matrix, QMatrix4x4 &cam)
{
    QMatrix4x4 matrixCopy = matrix;
    m_program->setUniformValue("textureSample", 1);

    paintGL_vertex_text_triangle(m_vertexBuffer_triangle_cylindre_vbo, m_textureBuffer_triangle_cylindre_vbo, m_normalBuffer_triangle_cylindre_vbo,
                                      0, total_tri_cylindre.stopModel[0], 0);
    paintGL_vertex_text_rectangle(m_vertexBuffer_rectangle_cylindre_vbo, m_textureBuffer_rectangle_cylindre_vbo, m_normalBuffer_rectangle_cylindre_vbo,
                                  m_exploBuffer_rectangle_cylindre_vbo, 0, total_rec_cylindre.stopModel[0], 0);
    m_program->setUniformValue("textureSample", 0);
    m_program->setUniformValue("changeColor", 0.f);
}

void GLArea::paintGL_vertex_color_triangle(QOpenGLBuffer &vertex,QOpenGLBuffer &color, QOpenGLBuffer &normal, int debut, int taille)
{
    vertex.bind();
    m_program->enableAttributeArray("posAttr");
    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);

    color.bind();
    m_program->enableAttributeArray("colAttr");
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 0, 3);

    normal.bind();
    m_program->enableAttributeArray("norAttr");
    m_program->setAttributeBuffer("norAttr", GL_FLOAT, 0, 3);

    glDrawArrays(GL_TRIANGLES, debut, taille);

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("colAttr");
    m_program->disableAttributeArray("norAttr");
}

void GLArea::paintGL_vertex_color_rectangle(QOpenGLBuffer &vertex,QOpenGLBuffer &color, QOpenGLBuffer &normal,  int debut, int taille)
{
    vertex.bind();
    m_program->enableAttributeArray("posAttr");
    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);

    color.bind();
    m_program->enableAttributeArray("colAttr");
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 0, 3);

    normal.bind();
    m_program->enableAttributeArray("norAttr");
    m_program->setAttributeBuffer("norAttr", GL_FLOAT, 0, 3);

    glDrawArrays(GL_QUADS, debut, taille);

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("colAttr");
    m_program->disableAttributeArray("norAttr");
}

void GLArea::paintGL_vertex_text_triangle(QOpenGLBuffer &vertex,QOpenGLBuffer &text, QOpenGLBuffer &normal, int debut, int taille, int nbText)
{
    vertex.bind();
    m_program->enableAttributeArray("posAttr");
    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);

    text.bind();
    m_program->enableAttributeArray("texAttr");
    m_program->setAttributeBuffer("texAttr", GL_FLOAT, 0, 2);

    normal.bind();
    m_program->enableAttributeArray("norAttr");
    m_program->setAttributeBuffer("norAttr", GL_FLOAT, 0, 3);


    m_textures[0]->bind();
    glDrawArrays(GL_TRIANGLES, debut, taille);
    m_textures[0]->release();

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("texAttr");
    m_program->disableAttributeArray("norAttr");
}

void GLArea::paintGL_vertex_text_rectangle(QOpenGLBuffer &vertex,QOpenGLBuffer &text, QOpenGLBuffer &normal,  QOpenGLBuffer &explo, int debut, int taille, int nbText)
{
    vertex.bind();
    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);
    m_program->enableAttributeArray("posAttr");

    text.bind();
    m_program->setAttributeBuffer("texAttr", GL_FLOAT, 0, 2);
    m_program->enableAttributeArray("texAttr");

    normal.bind();
    m_program->enableAttributeArray("norAttr");
    m_program->setAttributeBuffer("norAttr", GL_FLOAT, 0, 3);


    explo.bind();
    m_program->setAttributeBuffer("exploAttr", GL_FLOAT, 0, 1);
    m_program->enableAttributeArray("exploAttr");

    m_textures[0]->bind();
    glDrawArrays(GL_QUADS, debut, taille);
    m_textures[0]->release();

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("texAttr");
    m_program->disableAttributeArray("norAttr");
    m_program->disableAttributeArray("exploAttr");
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        case Qt::Key_Space :
            m_angle += 1;
            if (m_angle >= 360) m_angle -= 360;
            update();
            break;
        case Qt::Key_A:
            if(m_vitesse_alpha <= 50)
                set_vitesse_change(m_vitesse_alpha + 1);
            break;
        case Qt::Key_R:
            if(m_vitesse_alpha > 1)
                set_vitesse_change(m_vitesse_alpha - 1);
            break;
        case Qt::Key_D:
            demarrer();
            break;
        case Qt::Key_S:
            stop();
            break;
    }
}

void GLArea::set_vitesse_change(int vitesse)
{
    m_vitesse_alpha = vitesse;
    emit vitesse_change(m_vitesse_alpha);
}

void GLArea::demarrer()
{
    if (m_timer->isActive())
        return;
    m_timer->start();
}

void GLArea::stop()
{
    if (!m_timer->isActive())
        return;
    m_timer->stop();
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::onTimeout()
{
    qDebug() << __FUNCTION__ ;
    m_anim += 0.01;
    m_alpha += m_vitesse_alpha;

    if (m_anim > 1) m_anim = 0;
    if (m_alpha > 360) m_alpha = 0;
    update();
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}
