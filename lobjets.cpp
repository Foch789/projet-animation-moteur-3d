#include "lobjets.h"
#include <math.h>
#include <QDebug>
#include <QVector3D>

#define PI 3.141592

LForme creer_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_cyl, LCouleur color)
{
    LTriangle faces = creer_face_cylindre(nb_fac, r_cyl, ep_cyl, color);
    LRectangle cotes = creer_cote_cylindre(nb_fac, r_cyl, ep_cyl, color);

    LForme cylindre;
    cylindre.forme_triangle = faces;
    cylindre.forme_rectangle = cotes;

    return cylindre;
}

LTriangle creer_face_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_rec, LCouleur color)
{
    GLfloat angle = (360.0f/nb_fac);
    GLfloat val = static_cast<GLfloat>(PI)/180.f;

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> normals;
    int nbSommet = 0;

    LTriangle result;

    auto temp = ep_rec/2.0f;
    for (int p = 0; p < 2 ;p++) {
        GLfloat angle_old = 0;
        GLfloat angle_actu = angle;

        for (int i = 0; i < nb_fac; i++) {
            GLfloat angle_rad_old = angle_old * val;
            GLfloat angle_rad = angle_actu * val;
            angle_old = angle_actu;
            angle_actu += angle;

            LVertex vertexA = LVertex();
            vertexA.x = 0.0f; vertexA.y = 0.0f; vertexA.z = temp;
            vertices.append(vertexA.x);
            vertices.append(vertexA.y);
            vertices.append(vertexA.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            nbSommet++;

            LVertex vertexF = LVertex();
            vertexF.x = r_cyl*cos(angle_rad_old); vertexF.y = r_cyl*sin(angle_rad_old); vertexF.z = temp;
            vertices.append(vertexF.x);
            vertices.append(vertexF.y);
            vertices.append(vertexF.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            nbSommet++;

            LVertex vertexC = LVertex();
            vertexC.x = r_cyl*cos(angle_rad); vertexC.y = r_cyl*sin(angle_rad); vertexC.z = temp;
            vertices.append(vertexC.x);
            vertices.append(vertexC.y);
            vertices.append(vertexC.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            nbSommet++;

            QVector<LVertex> aideNor;
            aideNor.append(vertexA);
            aideNor.append(vertexF);
            aideNor.append(vertexC);


            QVector3D vAF(aideNor[1].x-aideNor[0].x, aideNor[1].y-aideNor[0].y, aideNor[1].z-aideNor[0].z);
            QVector3D vAC(aideNor[2].x-aideNor[0].x, aideNor[2].y-aideNor[0].y, aideNor[2].z-aideNor[0].z);

            QVector3D nAFC = QVector3D::normal(vAF, vAC);
            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

        }
        temp = -temp;
    }

    result.vertices = vertices;
    result.colors = colors;
    result.normals = normals;
    result.nbVertices = nbSommet;

    return result;
}

LRectangle creer_cote_cylindre(int nb_fac, GLfloat r_cyl, GLfloat ep_cyl, LCouleur color)
{

    GLfloat angle = (360.0f/nb_fac);
    GLfloat val = static_cast<GLfloat>(PI)/180.f;

    color.r *= 0.8f;
    color.g *= 0.8f;
    color.b *= 0.8f;

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> normals;
    int nbSommet = 0;

    LRectangle result;

    GLfloat angle_old = 0;
    GLfloat angle_actu = angle;

    for (int i = 0; i < nb_fac; i++) {
        GLfloat angle_rad_old = angle_old * val;
        GLfloat angle_rad = angle_actu * val;
        angle_old = angle_actu;
        angle_actu += angle;

        LVertex vertexF = LVertex();
        vertexF.x = r_cyl*cos(angle_rad_old); vertexF.y = r_cyl*sin(angle_rad_old); vertexF.z = ep_cyl/2.0f;
        vertices.append(vertexF.x);
        vertices.append(vertexF.y);
        vertices.append(vertexF.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexC = LVertex();
        vertexC.x = r_cyl*cos(angle_rad); vertexC.y = r_cyl*sin(angle_rad); vertexC.z = ep_cyl/2.0f;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexD = LVertex();
        vertexD.x = r_cyl*cos(angle_rad); vertexD.y = r_cyl*sin(angle_rad); vertexD.z = -ep_cyl/2.0f;
        vertices.append(vertexD.x);
        vertices.append(vertexD.y);
        vertices.append(vertexD.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexE = LVertex();
        vertexE.x = r_cyl*cos(angle_rad_old); vertexE.y = r_cyl*sin(angle_rad_old); vertexE.z = -ep_cyl/2.0f;
        vertices.append(vertexE.x);
        vertices.append(vertexE.y);
        vertices.append(vertexE.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexE);
        aideNor.append(vertexF);
        aideNor.append(vertexC);
        aideNor.append(vertexD);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

    }

    result.vertices = vertices;
    result.colors = colors;
    result.normals = normals;
    result.nbVertices = nbSommet;

    return result;
}

LForme creer_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color)
{
    LForme faces = creer_face_demi_cylindre(nb_fac, r_cyl, long_cyl, ep_cyl, color);
    LRectangle cotes = creer_cote_demi_cylindre(nb_fac, r_cyl, long_cyl, ep_cyl, color);

    LForme demi_cylindre;
    demi_cylindre.forme_triangle = faces.forme_triangle;
    demi_cylindre.forme_rectangle = faces.forme_rectangle;
    demi_cylindre.forme_rectangle += cotes;

    return demi_cylindre;
}

LForme creer_face_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color)
{
    GLfloat angle = (360.0f/nb_fac);
    GLfloat val = static_cast<GLfloat>(PI)/180.f;

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> textures;
    QVector<GLfloat> normals;
    QVector<GLfloat> concernExplosion;

    int nbSommet = 0;

    GLfloat pas_text = 1.f/static_cast<GLfloat>(nb_fac);

    GLfloat pgbx = 0.f;
    GLfloat pgby = 0.f;

    GLfloat pdbx = pas_text;
    GLfloat pdby = 0.f;

    GLfloat pdhx = pas_text;
    GLfloat pdhy = 1.f;

    GLfloat pghx = 0.f;
    GLfloat pghy = 1.f;

    LForme result;

    GLfloat dist_center = r_cyl + ep_cyl;

    auto temp = long_cyl/2.0f;
    GLfloat angle_old = 0;
    GLfloat angle_actu = angle;

    for (int i = 0; i < nb_fac; i++) {
        GLfloat angle_rad_old = angle_old * val;
        GLfloat angle_rad = angle_actu * val;
        angle_old = angle_actu;
        angle_actu += angle;


        LVertex vertexA = LVertex();
        vertexA.x = 0.0f; vertexA.y = 0.0f; vertexA.z = temp;
        vertices.append(vertexA.x);
        vertices.append(vertexA.y);
        vertices.append(vertexA.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;


        LVertex vertexF = LVertex();
        vertexF.x = dist_center*cos(angle_rad_old); vertexF.y = dist_center*sin(angle_rad_old); vertexF.z = temp;
        vertices.append(vertexF.x);
        vertices.append(vertexF.y);
        vertices.append(vertexF.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        if(angle_actu > 180)
        {
            angle_rad = 180 * val;
            LVertex vertexC = LVertex();
            vertexC.x = dist_center*cos(angle_rad); vertexC.y = dist_center*sin(angle_rad); vertexC.z = temp;
            vertices.append(vertexC.x);
            vertices.append(vertexC.y);
            vertices.append(vertexC.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            nbSommet++;

            QVector<LVertex> aideNor;
            aideNor.append(vertexA);
            aideNor.append(vertexF);
            aideNor.append(vertexC);

            QVector3D vAF(aideNor[1].x-aideNor[0].x, aideNor[1].y-aideNor[0].y, aideNor[1].z-aideNor[0].z);
            QVector3D vAC(aideNor[2].x-aideNor[0].x, aideNor[2].y-aideNor[0].y, aideNor[2].z-aideNor[0].z);

            QVector3D nAFC = QVector3D::normal(vAF, vAC);
            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

            normals.append(nAFC.x());
            normals.append(nAFC.y());
            normals.append(nAFC.z());

            break;
        }

        LVertex vertexC = LVertex();
        vertexC.x = dist_center*cos(angle_rad); vertexC.y = dist_center*sin(angle_rad); vertexC.z = temp;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexA);
        aideNor.append(vertexF);
        aideNor.append(vertexC);

        QVector3D vAF(aideNor[1].x-aideNor[0].x, aideNor[1].y-aideNor[0].y, aideNor[1].z-aideNor[0].z);
        QVector3D vAC(aideNor[2].x-aideNor[0].x, aideNor[2].y-aideNor[0].y, aideNor[2].z-aideNor[0].z);

        QVector3D nAFC = QVector3D::normal(vAF, vAC);
        normals.append(nAFC.x());
        normals.append(nAFC.y());
        normals.append(nAFC.z());

        normals.append(nAFC.x());
        normals.append(nAFC.y());
        normals.append(nAFC.z());

        normals.append(nAFC.x());
        normals.append(nAFC.y());
        normals.append(nAFC.z());
    }

    result.forme_triangle.vertices = vertices;
    result.forme_triangle.colors = colors;
    result.forme_triangle.normals = normals;
    result.forme_triangle.nbVertices = nbSommet;

    colors.clear();
    vertices.clear();
    nbSommet = 0;

    temp = -temp;

    angle_old = 0;
    angle_actu = angle;
    for (int i = 0; i < nb_fac; i++) {
        GLfloat angle_rad_old = angle_old * val;
        GLfloat angle_rad = angle_actu * val;
        angle_old = angle_actu;
        angle_actu += angle;

        LVertex vertexF = LVertex();
        vertexF.x = r_cyl*cos(angle_rad_old); vertexF.y = r_cyl*sin(angle_rad_old); vertexF.z = temp;
        vertices.append(vertexF.x);
        vertices.append(vertexF.y);
        vertices.append(vertexF.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pgbx);
        textures.append(pgby);
        concernExplosion.append(0.f);
        nbSommet++;


        if(angle_actu > 180)
        {

            angle_rad = 180 * val;
            LVertex vertexC = LVertex();
            vertexC.x = r_cyl*cos(angle_rad); vertexC.y = r_cyl*sin(angle_rad); vertexC.z = temp;
            vertices.append(vertexC.x);
            vertices.append(vertexC.y);
            vertices.append(vertexC.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pdbx);
            textures.append(pdby);
            concernExplosion.append(0.f);
            nbSommet++;

            LVertex vertexCrec = LVertex();
            vertexCrec.x = dist_center*cos(angle_rad); vertexCrec.y = dist_center*sin(angle_rad); vertexCrec.z = temp;
            vertices.append(vertexCrec.x);
            vertices.append(vertexCrec.y);
            vertices.append(vertexCrec.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pdhx);
            textures.append(pdhy);
            concernExplosion.append(0.f);
            nbSommet++;

            LVertex vertexFrec = LVertex();
            vertexFrec.x = dist_center*cos(angle_rad_old); vertexFrec.y = dist_center*sin(angle_rad_old); vertexFrec.z = temp;
            vertices.append(vertexFrec.x);
            vertices.append(vertexFrec.y);
            vertices.append(vertexFrec.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pghx);
            textures.append(pghy);
            concernExplosion.append(0.f);
            nbSommet++;

            QVector<LVertex> aideNor;
            aideNor.append(vertexF);
            aideNor.append(vertexC);
            aideNor.append(vertexCrec);
            aideNor.append(vertexFrec);

            QVector3D normal(0,0,0);

            for (int i=0; i<4; i++)
            {
                int j = (i+1)%4;
                normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
                normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
                normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
            }
            //normal.normalize();

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            break;
        }

        LVertex vertexC = LVertex();
        vertexC.x = r_cyl*cos(angle_rad); vertexC.y = r_cyl*sin(angle_rad); vertexC.z = temp;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pdbx);
        textures.append(pdby);
        concernExplosion.append(0.f);
        nbSommet++;

        LVertex vertexCrec = LVertex();
        vertexCrec.x = dist_center*cos(angle_rad); vertexCrec.y = dist_center*sin(angle_rad); vertexCrec.z = temp;
        vertices.append(vertexCrec.x);
        vertices.append(vertexCrec.y);
        vertices.append(vertexCrec.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pdhx);
        textures.append(pdhy);
        concernExplosion.append(0.f);
        nbSommet++;

        LVertex vertexFrec = LVertex();
        vertexFrec.x = dist_center*cos(angle_rad_old); vertexFrec.y = dist_center*sin(angle_rad_old); vertexFrec.z = temp;
        vertices.append(vertexFrec.x);
        vertices.append(vertexFrec.y);
        vertices.append(vertexFrec.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pghx);
        textures.append(pghy);
        concernExplosion.append(0.f);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexF);
        aideNor.append(vertexC);
        aideNor.append(vertexCrec);
        aideNor.append(vertexFrec);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        pgbx += pas_text;
        pdbx += pas_text;
        pdhx += pas_text;
        pghx += pas_text;
    }

    result.forme_rectangle.vertices = vertices;
    result.forme_rectangle.colors = colors;
    result.forme_rectangle.textures = textures;
    result.forme_rectangle.nbVertices = nbSommet;
    result.forme_rectangle.normals = normals;
    result.forme_rectangle.concernExplosion = concernExplosion;

    return result;
}

LRectangle creer_cote_demi_cylindre(int nb_fac, GLfloat r_cyl, GLfloat long_cyl, GLfloat ep_cyl, LCouleur color)
{
    GLfloat angle = (360.0f/nb_fac);
    GLfloat val = static_cast<GLfloat>(PI)/180.f;

    color.r *= 0.8f;
    color.g *= 0.8f;
    color.b *= 0.8f;

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> textures;
    QVector<GLfloat> normals;
    QVector<GLfloat> concernExplosion;
    int nbSommet = 0;

    GLfloat pas_text = 1.f/static_cast<GLfloat>(nb_fac);

    GLfloat pgbx = 0.f;
    GLfloat pgby = 0.f;

    GLfloat pdbx = pas_text;
    GLfloat pdby = 0.f;

    GLfloat pdhx = pas_text;
    GLfloat pdhy = 1.f;

    GLfloat pghx = 0.f;
    GLfloat pghy = 1.f;

    LRectangle result;

    GLfloat angle_old = 0;
    GLfloat angle_actu = angle;

    GLfloat dist_change =  r_cyl;

    for(int p = 0; p < 2; p++)
    {
        angle_old = 0;
        angle_actu = angle;
        for (int i = 0; i < nb_fac; i++) {
            GLfloat angle_rad_old = angle_old * val;
            GLfloat angle_rad = angle_actu * val;
            angle_old = angle_actu;
            angle_actu += angle;

            LVertex vertexF = LVertex();
            vertexF.x = dist_change*cos(angle_rad_old); vertexF.y = dist_change*sin(angle_rad_old); vertexF.z = long_cyl/2.0f;
            vertices.append(vertexF.x);
            vertices.append(vertexF.y);
            vertices.append(vertexF.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pgbx);
            textures.append(pgby);
            if(dist_change < r_cyl + ep_cyl)
            {
                concernExplosion.append(2.f);
            }
            else
            {
                concernExplosion.append(0.f);
            }
            nbSommet++;

            if(angle_actu > 180)
            {
                angle_rad = 180 * val;

                LVertex vertexC = LVertex();
                vertexC.x = dist_change*cos(angle_rad); vertexC.y = dist_change*sin(angle_rad); vertexC.z = long_cyl/2.0f;
                vertices.append(vertexC.x);
                vertices.append(vertexC.y);
                vertices.append(vertexC.z);
                colors.append(color.r);
                colors.append(color.g);
                colors.append(color.b);
                textures.append(pdbx);
                textures.append(pdby);
                if(dist_change < r_cyl + ep_cyl)
                {
                    concernExplosion.append(2.f);
                }
                else
                {
                    concernExplosion.append(0.f);
                }
                nbSommet++;

                LVertex vertexD = LVertex();
                vertexD.x = dist_change*cos(angle_rad); vertexD.y = dist_change*sin(angle_rad); vertexD.z = -long_cyl/2.0f;
                vertices.append(vertexD.x);
                vertices.append(vertexD.y);
                vertices.append(vertexD.z);
                colors.append(color.r);
                colors.append(color.g);
                colors.append(color.b);
                textures.append(pdhx);
                textures.append(pdhy);
                concernExplosion.append(0.f);
                nbSommet++;

                LVertex vertexE = LVertex();
                vertexE.x = dist_change*cos(angle_rad_old); vertexE.y = dist_change*sin(angle_rad_old); vertexE.z = -long_cyl/2.0f;
                vertices.append(vertexE.x);
                vertices.append(vertexE.y);
                vertices.append(vertexE.z);
                colors.append(color.r);
                colors.append(color.g);
                colors.append(color.b);
                textures.append(pghx);
                textures.append(pghy);
                concernExplosion.append(0.f);
                nbSommet++;

                QVector<LVertex> aideNor;
                aideNor.append(vertexF);
                aideNor.append(vertexC);
                aideNor.append(vertexD);
                aideNor.append(vertexE);

                QVector3D normal(0,0,0);

                for (int i=0; i<4; i++)
                {
                    int j = (i+1)%4;
                    normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
                    normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
                    normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
                }
                //normal.normalize();

                normals.append(normal.x());
                normals.append(normal.y());
                normals.append(normal.z());

                normals.append(normal.x());
                normals.append(normal.y());
                normals.append(normal.z());

                normals.append(normal.x());
                normals.append(normal.y());
                normals.append(normal.z());

                normals.append(normal.x());
                normals.append(normal.y());
                normals.append(normal.z());

                break;
            }

            LVertex vertexC = LVertex();
            vertexC.x = dist_change*cos(angle_rad); vertexC.y = dist_change*sin(angle_rad); vertexC.z = long_cyl/2.0f;
            vertices.append(vertexC.x);
            vertices.append(vertexC.y);
            vertices.append(vertexC.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pdbx);
            textures.append(pdby);
            if(dist_change < r_cyl + ep_cyl)
            {
                concernExplosion.append(2.f);
            }
            else
            {
                concernExplosion.append(0.f);
            }
            nbSommet++;

            LVertex vertexD = LVertex();
            vertexD.x = dist_change*cos(angle_rad); vertexD.y = dist_change*sin(angle_rad); vertexD.z = -long_cyl/2.0f;
            vertices.append(vertexD.x);
            vertices.append(vertexD.y);
            vertices.append(vertexD.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pdhx);
            textures.append(pdhy);
            concernExplosion.append(0.f);
            nbSommet++;

            LVertex vertexE = LVertex();
            vertexE.x = dist_change*cos(angle_rad_old); vertexE.y = dist_change*sin(angle_rad_old); vertexE.z = -long_cyl/2.0f;
            vertices.append(vertexE.x);
            vertices.append(vertexE.y);
            vertices.append(vertexE.z);
            colors.append(color.r);
            colors.append(color.g);
            colors.append(color.b);
            textures.append(pghx);
            textures.append(pghy);
            concernExplosion.append(0.f);
            nbSommet++;

            QVector<LVertex> aideNor;
            aideNor.append(vertexF);
            aideNor.append(vertexC);
            aideNor.append(vertexD);
            aideNor.append(vertexE);

            QVector3D normal(0,0,0);

            for (int i=0; i<4; i++)
            {
                int j = (i+1)%4;
                normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
                normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
                normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
            }
            //normal.normalize();

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());

            pgbx += pas_text;
            pdbx += pas_text;
            pdhx += pas_text;
            pghx += pas_text;

        }
        dist_change += ep_cyl;
    }

    pgbx = 0.f;
    pdbx = pas_text;
    pdhx = pas_text;
    pghx = 0.f;


    for(int i = 0; i < 2; i++)
    {
        LVertex vertexF = LVertex();
        vertexF.x = r_cyl ; vertexF.y = 0.f; vertexF.z = long_cyl/2.0f;
        vertices.append(vertexF.x);
        vertices.append(vertexF.y);
        vertices.append(vertexF.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pgbx);
        textures.append(pgby);
        concernExplosion.append(0.f);
        nbSommet++;

        LVertex vertexC = LVertex();
        vertexC.x = r_cyl + ep_cyl; vertexC.y = 0.f; vertexC.z = long_cyl/2.0f;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pdbx);
        textures.append(pdby);
        concernExplosion.append(0.f);
        nbSommet++;

        LVertex vertexD = LVertex();
        vertexD.x = r_cyl + ep_cyl; vertexD.y = 0.f; vertexD.z = -long_cyl/2.0f;
        vertices.append(vertexD.x);
        vertices.append(vertexD.y);
        vertices.append(vertexD.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pdhx);
        textures.append(pdhy);
        concernExplosion.append(0.f);
        nbSommet++;

        LVertex vertexE = LVertex();
        vertexE.x = r_cyl; vertexE.y = 0.f; vertexE.z = -long_cyl/2.0f;
        vertices.append(vertexE.x);
        vertices.append(vertexE.y);
        vertices.append(vertexE.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        textures.append(pghx);
        textures.append(pghy);
        concernExplosion.append(0.f);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexF);
        aideNor.append(vertexC);
        aideNor.append(vertexD);
        aideNor.append(vertexE);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        pgbx += pas_text;
        pdbx += pas_text;
        pdhx += pas_text;
        pghx += pas_text;

        r_cyl = -r_cyl;
        ep_cyl = -ep_cyl;
    }
    r_cyl = -r_cyl;
    ep_cyl = -ep_cyl;

    result.vertices = vertices;
    result.colors = colors;
    result.textures = textures;
    result.normals = normals;
    result.nbVertices = nbSommet;
    result.concernExplosion = concernExplosion;

    return result;
}

LRectangle creer_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color)
{

    LRectangle faces = creer_face_rectangle(long_rec, larg_rec, ep_rec, color);
    LRectangle cotes = creer_cote_rectangle(long_rec, larg_rec, ep_rec, color);

    LRectangle rectangle;
    rectangle += faces;
    rectangle += cotes;

    return rectangle;
}

LRectangle creer_face_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color)
{

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> normals;
    int nbSommet = 0;

    LRectangle result;

    auto temp = ep_rec/2.0f;

    for (int i = 0; i < 2; i++) {

        LVertex vertexA = LVertex();
        vertexA.x = 0.0f; vertexA.y = 0.0f; vertexA.z = temp;
        vertices.append(vertexA.x);
        vertices.append(vertexA.y);
        vertices.append(vertexA.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexB = LVertex();
        vertexB.x = 0.0f; vertexB.y = larg_rec; vertexB.z = temp;
        vertices.append(vertexB.x);
        vertices.append(vertexB.y);
        vertices.append(vertexB.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexC = LVertex();
        vertexC.x = long_rec; vertexC.y = larg_rec; vertexC.z = temp;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexD = LVertex();
        vertexD.x = long_rec; vertexD.y = 0.0f; vertexD.z = temp;
        vertices.append(vertexD.x);
        vertices.append(vertexD.y);
        vertices.append(vertexD.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexA);
        aideNor.append(vertexB);
        aideNor.append(vertexC);
        aideNor.append(vertexD);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        temp = -ep_rec/2.0f;
    }

    result.vertices = vertices;
    result.colors = colors;
    result.nbVertices = nbSommet;
    result.normals = normals;

    return result;

}

LRectangle creer_cote_rectangle(GLfloat long_rec, GLfloat larg_rec, GLfloat ep_rec, LCouleur color)
{

    color.r *= 0.8f;
    color.g *= 0.8f;
    color.b *= 0.8f;

    QVector<GLfloat> colors;
    QVector<GLfloat> vertices;
    QVector<GLfloat> normals;
    int nbSommet = 0;

    LRectangle result;

    auto temp = 0.0f;

    for (int i = 0; i < 2; i++) {

        LVertex vertexA = LVertex();
        vertexA.x = temp; vertexA.y = 0.0f; vertexA.z = ep_rec/2.0f;
        vertices.append(vertexA.x);
        vertices.append(vertexA.y);
        vertices.append(vertexA.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;


        LVertex vertexB = LVertex();
        vertexB.x = temp; vertexB.y = larg_rec; vertexB.z = ep_rec/2.0f;
        vertices.append(vertexB.x);
        vertices.append(vertexB.y);
        vertices.append(vertexB.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;


        LVertex vertexC = LVertex();
        vertexC.x = temp; vertexC.y = larg_rec; vertexC.z = -ep_rec/2.0f;
        vertices.append(vertexC.x);
        vertices.append(vertexC.y);
        vertices.append(vertexC.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;


        LVertex vertexD = LVertex();
        vertexD.x = temp; vertexD.y = 0.0f; vertexD.z = -ep_rec/2.0f;
        vertices.append(vertexD.x);
        vertices.append(vertexD.y);
        vertices.append(vertexD.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexC);
        aideNor.append(vertexB);
        aideNor.append(vertexA);
        aideNor.append(vertexD);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        temp = long_rec;
    }

    temp = 0.0;

    for (int i = 0; i < 2; i++) {

        LVertex vertexE = LVertex();
        vertexE.x = 0.0f; vertexE.y = temp; vertexE.z = ep_rec/2.0f;
        vertices.append(vertexE.x);
        vertices.append(vertexE.y);
        vertices.append(vertexE.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexF = LVertex();
        vertexF.x = long_rec; vertexF.y = temp; vertexF.z = ep_rec/2.0f;
        vertices.append(vertexF.x);
        vertices.append(vertexF.y);
        vertices.append(vertexF.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexG = LVertex();
        vertexG.x = long_rec; vertexG.y = temp; vertexG.z = -ep_rec/2.0f;
        vertices.append(vertexG.x);
        vertices.append(vertexG.y);
        vertices.append(vertexG.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        LVertex vertexH = LVertex();
        vertexH.x = 0.0f; vertexH.y = temp; vertexH.z = -ep_rec/2.0f;
        vertices.append(vertexH.x);
        vertices.append(vertexH.y);
        vertices.append(vertexH.z);
        colors.append(color.r);
        colors.append(color.g);
        colors.append(color.b);
        nbSommet++;

        QVector<LVertex> aideNor;
        aideNor.append(vertexE);
        aideNor.append(vertexF);
        aideNor.append(vertexG);
        aideNor.append(vertexH);

        QVector3D normal(0,0,0);

        for (int i=0; i<4; i++)
        {
            int j = (i+1)%4;
            normal.setX(normal.x() + ((aideNor[i].y - aideNor[j].y)*(aideNor[i].z + aideNor[j].z)));
            normal.setY(normal.y() + ((aideNor[i].z - aideNor[j].z)*(aideNor[i].x + aideNor[j].x)));
            normal.setZ(normal.z() + ((aideNor[i].x - aideNor[j].x)*(aideNor[i].y + aideNor[j].y)));
        }
        //normal.normalize();

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        normals.append(normal.x());
        normals.append(normal.y());
        normals.append(normal.z());

        temp = larg_rec;
    }

    result.vertices = vertices;
    result.colors = colors;
    result.nbVertices = nbSommet;
    result.normals = normals;

    return result;
}
