#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T12:27:11
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TP4-Laurent
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
        lobjets.cpp \
        princ.cpp \
        glarea.cpp

HEADERS  += princ.h \
        dialog.h \
        glarea.h \
        lobjets.h

FORMS    += princ.ui \
    dialog.ui

RESOURCES += \
    TP4-Laurent.qrc
